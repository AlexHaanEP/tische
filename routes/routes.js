const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('../client/index', {user: req.query});
});

module.exports = router;
