const express = require('express');
const router = express.Router();
const server = require('../app');

router.post('/login', (req, res, next) => {
    console.log(req.body);
    server.createUser(req.body.name, req.body.password, next);
    res.render('../client/success', {user: {name: req.body.name}});
});

module.exports = router;