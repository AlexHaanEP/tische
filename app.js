const express = require('express');
const database = require('./database');
const bodyParser = require('body-parser');
const routes = require('./routes/routes');
const login = require('./routes/login');
const app = express();

const port = 8080;

app.set('view engine', 'ejs');
app.use('/client', express.static('client'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/', routes);
app.use('/', login);


database.query('SELECT * FROM users', (err, res) => {
    console.log(err, res);
});

exports.createUser = (name, password, next) => {
    database.query('INSERT INTO users(name, password) VALUES ($1,$2);',
        [name, password], (err) => {
            if (err) {
                console.log(err);
                return next(err);
            }
        });
};

app.listen(port);