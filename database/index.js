const { Pool, Client } = require('pg');
const connectionString = 'postgresql://dbadmin:SuperArbit3r@127.0.0.1:5432/dbadmin';

const pool = new Pool({
    connectionString: connectionString,
});
pool.connect();

const client = new Client({
    connectionString: connectionString,
});
client.connect();

module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback);
    },
    getClient: (callback) => {
        pool.connect((err, client, done) => {
            callback(err, client, done);
        })
    }
};

